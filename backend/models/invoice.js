import mongoose from "mongoose";

const invoiceShema = mongoose.Schema({
    client_ID : {
        type : mongoose.Schema.Types.ObjectId ,
        required : true ,
        ref : "users"
    },
    amount : {
        type : Number ,
        required : true
    } ,
    dueDate : {
        type : Date ,
        required : true 
    } ,
    status : {
        type : String ,
        default : false ,
        required : true
    }
} , {
    collection : "invoices" ,
    timestamps : true
})


export const invoiceModel = mongoose.model("invoices" , invoiceShema)
import mongoose from 'mongoose'
const userSchema = new mongoose.Schema({

  is_admin: {
    type: Boolean,
    default: false
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  phone_number: {
    type: String,
    required: true
  },
  firstname: {
    type: String,
    required: true
  },
  lastname: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  score: {
    type: Number,
    default: 0
  },
  subscription_id:{
    type : mongoose.Schema.Types.ObjectId 
  }
} , {
  timestamps : true
});

export const User = mongoose.model('User', userSchema);


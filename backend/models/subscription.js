import mongoose from "mongoose";

const subscriptionShema = mongoose.Schema({
    planName : {
        type : String ,
        required : true
    } ,
    features : {
        type : [String] ,
        required : true 
    } ,
    price : {
        type : Number ,
        required : true
    }
} , {
    collection : "subscriptions" ,
    timestamps : true
})


export const subscriptionModel = mongoose.model("subscriptions" , subscriptionShema)
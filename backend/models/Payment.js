import mongoose from "mongoose";

const paymentShema = mongoose.Schema({
    invoice_id :{
        type : mongoose.Schema.Types.ObjectId,
        required : true
    },
    amount : {
        type : Number ,
        required : true
    } ,
    date : {
        type : Date ,
        required : true 
    } ,
    status : {
        type : String ,
        default : false ,
        required : true
    }
} , {
    collection : "payments" ,
    timestamps : true
})


export const paymentModel = mongoose.model("payments" , paymentShema)
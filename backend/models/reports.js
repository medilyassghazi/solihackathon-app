import  mongoose from  'mongoose';

const reportSchema = new mongoose.Schema({
  client_id: {
    type: mongoose.Schema.Types.ObjectId ,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  response: {
    type: String,
    default: ''
  }
} , {
  timestamps : true
});

export const Report = mongoose.model('Report', reportSchema);


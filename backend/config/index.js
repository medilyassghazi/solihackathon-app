import mongoose from "mongoose" ;


export const Connect_to_mongodb = (CONNECTION_URL) => {
    mongoose.set("strictQuery" , true) ;
    mongoose.connect(CONNECTION_URL , {
        useNewUrlParser : true ,
        useUnifiedTopology : true ,
    })
    .then(async () => {
        console.log(`MONGODB server started on Port ${mongoose.connection.port}` )
    })
    .catch((error) => console.error(error.message))
}
import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import dotenv from "dotenv";
import helmet from "helmet"
import morgan from "morgan";
import { Connect_to_mongodb } from "./config/index.js";
import invoicesRouter from "./routes/invoices.js"
import paymentsRouter from "./routes/payments.js"
import {UserRouter} from './routes/user.js';



// generate a secret key
// console.log(crypto.randomBytes(48).toString("hex"))


// Get the variables environnement
dotenv.config()

// Configuration
const app = express();
app.use(helmet());
app.use(helmet.crossOriginResourcePolicy({ policy: "cross-origin" }));;
app.use(morgan("common"))
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ limit: "50mb", extended: true }))
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(cors());

//
app.use("/api/invoices" , invoicesRouter) 
app.use("/api/payments" , paymentsRouter) 
app.use("/*", (req, res) => res.status(404).json("Not found"))

//
app.use('/api/user',UserRouter)
 backend/server.js
// connect to mongodb
const MONGODB_URL = process.env.MONGO_URL;
Connect_to_mongodb(MONGODB_URL);
// Start the server
const PORT = process.env.PORT || 3334;
app.use("/*", (req, res) => res.status(404).json("Not found"))

app.listen(PORT, () => console.log(`Server started on Port : ${PORT}`))

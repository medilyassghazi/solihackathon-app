import express from "express";
const router = express.Router();
import {invoiceModel} from "../models/invoice.js"

// get invoices
router.get("/" , async (req , res)=>{
    try {
        const invoices = await invoiceModel.find({});
        res.status(200).json({invoices})
    } catch (error) {
        res.status(500).json({msg : error})
    }
})

// get invoices for a specific client
router.post("/:client_id" , async (req , res)=> {
    try {
        const {client_id} = req.params;
        const invoices = await invoiceModel.find({client_ID : client_id});
        res.status(200).json({invoices})
    } catch (error) {
        res.status(500).json({msg : error})
    }
})





export default router ;
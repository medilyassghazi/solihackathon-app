import express from "express";
const router = express.Router();
import {paymentModel} from "../models/Payment.js"


// get payments for a specific invoice
router.get("/:invoice_id" , async (req , res)=>{
    try {
        const {invoice_id} = req.params;
        const payments = await paymentModel.find({invoice_id : invoice_id})
        res.status(200).json({payments});
    } catch (error) {
        res.status(500).json({msg : error});
    }
})
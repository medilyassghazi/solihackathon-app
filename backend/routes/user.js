import express from "express";
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import {User} from '../models/user.js'; // Path to the user model file
import {invoiceModel} from '../models/invoice.js'; // Path to the invoice model file

export const UserRouter = express.Router();

// User registration route
UserRouter.post('/register', async (req, res) => {
  try {
    const { username, email, password } = req.body;

    // Check if a user with the same username or email already exists
    const existingUser = await User.findOne({ $or: [{ username }, { email }] });
    if (existingUser) {
      return res.status(400).json({ error: 'Username or email already exists' });
    }

    // Hash the password before saving
    const hashedPassword = await bcrypt.hash(password, 10);

    // Create a new user
    const newUser = new User({
      username,
      email,
      password: hashedPassword
    });

    await newUser.save();

    res.status(201).json({ message: 'User registered successfully' });
  } catch (error) {
    res.status(500).json({ error: 'Failed to register user' });
  }
});

// User login route
UserRouter.post('/login', async (req, res) => {
  try {
    const { username, password } = req.body;

    // Check if the user exists in the database
    const user = await User.findOne({ username });
    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }

    // Compare the provided password with the stored hashed password
    const passwordMatch = await bcrypt.compare(password, user.password);
    if (!passwordMatch) {
      return res.status(401).json({ error: 'Invalid password' });
    }

    // Generate a JWT token
    const token = jwt.sign({ userId: user._id }, 'your_secret_key');

    res.status(200).json({ message: 'User logged in successfully', token });
  } catch (error) {
    res.status(500).json({ error: 'Failed to log in' });
  }
});

UserRouter.get('/payment-today', async (req, res) => {
    try {
      // Get today's date
      const today = new Date();
      today.setHours(0, 0, 0, 0);
  
      // Find invoices with due date today
      const invoices = await invoiceModel.find({ dueDate: today });
  
      // Get the client IDs from the invoices
      const clientIDs = invoices.map((invoice) => invoice.client_ID);
  
      // Find the users with the matching client IDs
      const users = await User.find({ _id: { $in: clientIDs } });
  
      res.status(200).json(users);
    } catch (error) {
      res.status(500).json({ error: 'Failed to fetch users with payment today' });
    }
  });

